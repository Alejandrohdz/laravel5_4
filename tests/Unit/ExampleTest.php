<?php

namespace Tests\Unit;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
	use DatabaseTransactions;
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testBasicTest()
	{
		$first = factory(Post::class)->create();
		$second = factory(Post::class)->create([
			'created_at' => \Carbon\Carbon::now()->subMonth()
		]);
		$third = factory(Post::class)->create([
			'created_at' => \Carbon\Carbon::now()->subMonths(3)
		]);

		$posts = Post::archives();

		$this->assertEquals([
			[
				"year" => $first->created_at->format('Y'),
    			"month" => $first->created_at->format('F'),
    			"published" => 1
			],
			[
				"year" => $second->created_at->format('Y'),
    			"month" => $second->created_at->format('F'),
    			"published" => 1
			],
			[
				"year" => $first->created_at->subMonths(3)->format('Y'),
    			"month" => $first->created_at->subMonths(3)->format('F'),
    			"published" => 1
			]
		], $posts);
	}

	public function createUser()
	{
		$this->assertTrue(true);
	}
}
