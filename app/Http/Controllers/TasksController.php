<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;

class TasksController extends Controller
{
    public function index()
    {
    	$tasks = Task::all();
		// $tasks = DB::table('tasks')->get();
		return view('task.index', compact('tasks'));
    }

    public function show(Task $task)
    {
    	//$task = Task::find($id);
		// $task = DB::table('tasks')->find($id);
		return view('task.show', compact('task'));
    }
}
