<div class="blog-masthead">
	<div class="container">
		<nav class="nav blog-nav">
			<a class="nav-link active" href="/">Home</a>
			<a class="nav-link" href="/posts/create">New Post</a>
			<a class="nav-link" href="#">Press</a>
			<a class="nav-link" href="#">New hires</a>
			<a class="nav-link" href="#">About</a>
			@if (Auth::check())
				<a href="" class="nav-link ml-auto">{{ Auth::user()->name }}</a>
				<a href="/logout" class="nav-link ml-auto">Logout</a>
			@endif
		</nav>
	</div>
</div>