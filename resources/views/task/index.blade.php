<html>
<head>
	<meta charset="UTF-8">
	<title>Tasks</title>
</head>
<body>
	<div>
		<ul>
			@foreach($tasks as $task)	
				<a href="/tasks/{{ $task->id }}">
					<li> {{ $task->body }} </li>
				</a>
			@endforeach
		</ul>
	</div>
</body>
</html>